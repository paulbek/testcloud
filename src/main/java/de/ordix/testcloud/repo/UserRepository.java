package de.ordix.testcloud.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import de.ordix.testcloud.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

}
