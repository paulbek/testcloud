package de.ordix.testcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.ordix.testcloud.entity.User;
import de.ordix.testcloud.repo.UserRepository;

@RestController
public class HelloController {

	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/greeting", method = RequestMethod.GET)
	public String index() {
		return "Greetings from OpenShift!";
	}

	@RequestMapping(value = "/users/add", method = RequestMethod.GET)
	public @ResponseBody String addNewUser(@RequestParam String name) {

		User n = new User();
		n.setName(name);
		userRepository.save(n);
		return "Saved";
	}
	
	@RequestMapping(value = "/users/all", method = RequestMethod.GET)
	public @ResponseBody Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}

}
